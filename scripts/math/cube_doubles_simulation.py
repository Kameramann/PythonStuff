import random

count_digits = int(input("Please enter the count of digits: "))
range_min = int(input("Please enter a min range: "))
range_max = int(input("Please enter a max range: "))

print("Range is from " + str(range_min) + " to " + str(range_max))

digits = list()


def generate_digits():
    for i in range(0, count_digits):
        digits.append(str(random.randrange(range_min, range_max))+'|'+str(random.randrange(range_min, range_max)))


def print_digits():
    table = ["## ## ## ## ## ## ## ## ## ##\n"]
    count = -1
    for digit in digits:

        count += 1
        if count == 10:
            count = 0
            table.append("\n")

        table.append(str(digit).replace("|", "") + ' ')
    table.append("\n## ## ## ## ## ## ## ## ## ##")
    print(''.join(table))


def calculate():
    double_count = 0
    for digit in digits:
        split_digit = str(digit).split("|")
        if split_digit[0] == split_digit[1]:
            double_count += 1
    print("Doubles: " + str(double_count))
    print("Probability: " + str((double_count/count_digits)*100) + "%")


generate_digits()
print_digits()
calculate()
